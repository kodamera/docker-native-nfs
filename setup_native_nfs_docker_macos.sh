#!/usr/bin/env bash

OS=`uname -s`

if [ $OS != "Darwin" ]; then
  echo "This script is macOS-only. Please do not run it on any other Unix."
  exit 1
fi

if [[ $EUID -eq 0 ]]; then
  echo "This script must NOT be run with sudo/root. Please re-run without sudo." 1>&2
  exit 1
fi

echo ""
echo " +--------------------------------+"
echo " | Setup native NFS for Docker 🐳 |"
echo " +--------------------------------+"
echo ""

echo "⚠️  WARNING: This script will:"
echo "- stop running containers"
echo "- reset file permissions recursively in this folder: 📁 $PWD"
echo "- add this folder to /etc/exports: 📁 $PWD"
echo ""
echo -n "Do you wish to proceed? [y]: "
read decision

if [ "$decision" != "y" ]; then
  echo "❌ Exiting. No changes made."
  exit 1
fi

echo ""

if ! docker ps > /dev/null 2>&1 ; then
  echo "== Waiting for docker to start..."
fi

open -a Docker

while ! docker ps > /dev/null 2>&1 ; do echo "." && sleep 2; done

echo "== Stopping running docker containers..."
docker stop $(docker ps -aq) > /dev/null 2>&1

osascript -e 'quit app "Docker"'

echo "== Resetting folder permissions..."
U=`id -u`
G=`id -g`
sudo chown -R "$U":"$G" .

echo "== Setting up nfs..."
LINE="$PWD -alldirs -mapall=$U:$G localhost"
FILE=/etc/exports
grep -qF -- "$LINE" "$FILE" || sudo echo "$LINE" | sudo tee -a $FILE > /dev/null

LINE="nfs.server.mount.require_resv_port = 0"
FILE=/etc/nfs.conf
grep -qF -- "$LINE" "$FILE" || sudo echo "$LINE" | sudo tee -a $FILE > /dev/null

echo "== Restarting nfsd..."
sudo nfsd restart

printf "== Restarting docker..."
open -a Docker

while ! docker ps > /dev/null 2>&1 ; do sleep 2 && printf "."; done

echo ""
echo ""
echo "✅ Success! Now go run your containers!"